package br.com.felipefaustini.data.repository

import br.com.felipefaustini.data.database.dao.UserDao
import br.com.felipefaustini.data.models.mappers.UserMapper
import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.domain.repository.UserRepository
import br.com.felipefaustini.domain.utils.ErrorEntity
import br.com.felipefaustini.domain.utils.Result
import kotlinx.coroutines.flow.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class UserRepositoryImpl @Inject constructor(
    private val userDao: UserDao,
    private val coroutineContext: CoroutineContext
) : UserRepository {

    override fun saveUser(user: User): Flow<Result<Boolean>> = flow {
        val data = UserMapper.map(user)
        val result = userDao.insert(data)
        if (result != -1L) {
            emit(Result.Success(true))
        } else {
            emit(Result.Error(ErrorEntity.Unknown))
        }
    }
        .catch {
            it.printStackTrace()
            emit(Result.Error(ErrorEntity.Unknown))
        }
        .flowOn(coroutineContext)

    override fun getAllUsers(): Flow<Result<List<User>>> = userDao.findAll()
        .map { Result.Success(it.map { userDB -> UserMapper.map(userDB) }) }
        .catch {
            it.printStackTrace()
            emit(Result.Success(emptyList()))
        }
        .flowOn(coroutineContext)

    override fun deleteById(id: Long): Flow<Result<Unit>> = flow<Result<Unit>> {
        userDao.deleteById(id)
        emit(Result.Success(Unit))
    }
        .catch {
            it.printStackTrace()
            emit(Result.Error(ErrorEntity.Unknown))
        }
        .flowOn(coroutineContext)

}