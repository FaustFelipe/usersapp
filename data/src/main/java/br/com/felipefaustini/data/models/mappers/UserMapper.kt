package br.com.felipefaustini.data.models.mappers

import br.com.felipefaustini.data.models.db.UserDB
import br.com.felipefaustini.domain.models.User

object UserMapper {

    fun map(userDB: UserDB) = User(
        id = userDB.id,
        name = userDB.name,
        avatar = userDB.avatar,
        bio = userDB.bio
    )

    fun map(user: User) = UserDB(
        name = user.name,
        avatar = user.avatar,
        bio = user.bio
    )

}