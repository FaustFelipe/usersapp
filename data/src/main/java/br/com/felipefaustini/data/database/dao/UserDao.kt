package br.com.felipefaustini.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.felipefaustini.data.database.dbutils.COLUMN_USER_DB_ID
import br.com.felipefaustini.data.database.dbutils.TABLE_USER_DB
import br.com.felipefaustini.data.models.db.UserDB
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(userDB: UserDB): Long

    @Query("SELECT * FROM $TABLE_USER_DB")
    fun findAll(): Flow<List<UserDB>>

    @Query("DELETE FROM $TABLE_USER_DB WHERE $COLUMN_USER_DB_ID = :id")
    suspend fun deleteById(id: Long)

}