package br.com.felipefaustini.data.models.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import br.com.felipefaustini.data.database.dbutils.*

@Entity(tableName = TABLE_USER_DB)
data class UserDB(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COLUMN_USER_DB_ID)
    var id: Long = 0,

    @ColumnInfo(name = COLUMN_USER_DB_NAME)
    var name: String,

    @ColumnInfo(name = COLUMN_USER_DB_AVATAR)
    var avatar: String,

    @ColumnInfo(name = COLUMN_USER_DB_BIO)
    var bio: String
)