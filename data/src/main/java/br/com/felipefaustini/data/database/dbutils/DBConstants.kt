package br.com.felipefaustini.data.database.dbutils

const val DB_NAME = "users_db"
const val DB_VERSION = 1

const val TABLE_USER_DB = "user_db"
const val COLUMN_USER_DB_ID = "_id"
const val COLUMN_USER_DB_NAME = "name"
const val COLUMN_USER_DB_AVATAR = "avatar"
const val COLUMN_USER_DB_BIO = "bio"