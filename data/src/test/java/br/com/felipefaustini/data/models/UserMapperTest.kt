package br.com.felipefaustini.data.models

import br.com.felipefaustini.data.models.db.UserDB
import br.com.felipefaustini.data.models.mappers.UserMapper
import br.com.felipefaustini.domain.models.User
import org.junit.Assert.assertEquals
import org.junit.Test

class UserMapperTest {

    @Test
    fun map_userDB_to_User() {
        val userDB = UserDB(
            id = 1,
            name = "Felipe",
            avatar = "",
            bio = "bio"
        )

        val user = UserMapper.map(userDB)
        assertEquals(1, user.id)
    }

    @Test
    fun map_user_to_userdb() {
        val user = User(
            name = "Felipe",
            avatar = "",
            bio = "bio"
        )

        val userDB = UserMapper.map(user)
        assertEquals("Felipe", userDB.name)
    }

}