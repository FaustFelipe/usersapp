package br.com.felipefaustini.data.repository

import br.com.felipefaustini.data.database.dao.UserDao
import br.com.felipefaustini.data.models.mappers.UserMapper
import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.domain.repository.UserRepository
import br.com.felipefaustini.domain.utils.ErrorEntity
import br.com.felipefaustini.domain.utils.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class UserRepositoryImplTest {

    @Mock
    private lateinit var userDao: UserDao

    private lateinit var repository: UserRepository

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun before_each_test() {
        repository = UserRepositoryImpl(
            userDao,
            dispatcher
        )
    }

    @Test
    fun saveUser_shouldReturnSuccessAfterSaved() = dispatcher.runBlockingTest {
        val expected = Result.Success(true)
        val user = User(
            name = "Felipe",
            avatar = "",
            bio = "bio"
        )
        val userDB = UserMapper.map(user)

        whenever(userDao.insert(userDB)).thenReturn(1L)

        val result = repository.saveUser(user).first()

        verify(userDao, times(1)).insert(userDB)
        assertEquals(expected, result)
    }

    @Test
    fun saveUser_shouldReturnErrorEntityAfterDBError() = dispatcher.runBlockingTest {
        val expected = Result.Error(ErrorEntity.Unknown)
        val user = User(
            name = "Felipe",
            avatar = "",
            bio = "bio"
        )
        val userDB = UserMapper.map(user)

        whenever(userDao.insert(userDB)).thenReturn(-1L)

        val result = repository.saveUser(user).first()

        verify(userDao, times(1)).insert(userDB)
        assertEquals(expected, result)
    }

    @Test
    fun getAllUsers_shouldReturnAListOfUsers() = dispatcher.runBlockingTest {
        val user = User(
            id = 0,
            name = "Felipe",
            avatar = "",
            bio = "bio"
        )
        val expected = Result.Success(listOf(user))

        whenever(userDao.findAll()).thenReturn(flowOf(listOf(user).map { UserMapper.map(it) }))

        val result = repository.getAllUsers().first()

        verify(userDao, times(1)).findAll()
        assertEquals(expected, result)
    }

}