package br.com.felipefaustini.usersapp.presentation.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.domain.usecases.home.HomeUseCase
import br.com.felipefaustini.domain.utils.Result
import br.com.felipefaustini.usersapp.MainCoroutineRule
import br.com.felipefaustini.usersapp.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class HomeViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var homeUseCase: HomeUseCase

    @InjectMocks
    private lateinit var homeViewModel: HomeViewModel

    @Test
    fun saveUser_shouldSaveUserSuccefully() = runBlockingTest {
        val expected = Unit
        homeViewModel.userImageUriCreation = null
        homeViewModel.username = name
        homeViewModel.bio = bio

        whenever(homeUseCase.saveUser(name, userImage, bio))
            .thenReturn(flowOf(Result.Success(true)))

        homeViewModel.saveUser()

        val result = homeViewModel.userSavedLiveData.getOrAwaitValue()

        assertEquals(expected, result)
    }

    @Test
    fun listUsers_shouldListAllUsers() = runBlockingTest {
        val expected = listOf<User>(User(id = 0, name, userImage, bio))

        whenever(homeUseCase.getUsers())
            .thenReturn(flowOf(Result.Success(listOf<User>(User(id = 0, name, userImage, bio)))))

        homeViewModel.listUsers()

        val result = homeViewModel.userListLiveData.getOrAwaitValue()

        assertEquals(expected, result)
    }

    companion object {
        private const val name = "Felipe"
        private const val bio = "bio"
        private const val userImage = ""
    }

}