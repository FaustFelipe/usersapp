package br.com.felipefaustini.usersapp.presentation.binding

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.usersapp.utils.extensions.showOrGoneInCondition

object ViewBinding {

    @JvmStatic
    @BindingAdapter("app:visibility")
    fun changeVisibility(view: View, condition: LiveData<List<User>>) {
        if (condition.value != null) {
            view.showOrGoneInCondition(condition.value!!.isEmpty())
        }
    }

}