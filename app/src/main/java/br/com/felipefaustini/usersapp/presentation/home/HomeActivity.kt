package br.com.felipefaustini.usersapp.presentation.home

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import br.com.felipefaustini.usersapp.R
import br.com.felipefaustini.usersapp.databinding.ActivityHomeBinding
import br.com.felipefaustini.usersapp.databinding.LyUserCreationDialogBinding
import br.com.felipefaustini.usersapp.presentation.BaseActivity
import br.com.felipefaustini.usersapp.presentation.binding.ImageBinding
import br.com.felipefaustini.usersapp.utils.extensions.inflate
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>(R.layout.activity_home) {

    private lateinit var alertUser: AlertDialog
    private lateinit var alertUserCreationBuilder: MaterialAlertDialogBuilder
    private var dialogCreatorBinding: LyUserCreationDialogBinding? = null

    private lateinit var galleryActivityResultLauncher: ActivityResultLauncher<Intent>

    override val viewModel: HomeViewModel by viewModels()

    private val usersAdapter = HomeUsersAdapter(
        onClickListener = {

        },
        onDeleteUserListener = {
            viewModel.deleteUser(it)
        }
    )

    override fun setupViews() {
        binding?.view = this
        binding?.viewModel = viewModel

        alertUserCreationBuilder = MaterialAlertDialogBuilder(this)

        binding?.rvUsers?.adapter = usersAdapter

        registerActivityResult()
    }

    override fun setupObservables() {
        super.setupObservables()

        viewModel.userSavedLiveData.observe(this) {
            if (alertUser.isShowing) {
                alertUser.dismiss()
            }
        }

        viewModel.userListLiveData.observe(this) {
            usersAdapter.setData(it)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            launchGallery()
        } else {
            showToast(getString(R.string.home_should_accept_permission))
        }
    }

    fun openDialogUserCreator(view: View) {
        dialogCreatorBinding = DataBindingUtil.bind<LyUserCreationDialogBinding>(
            inflate(R.layout.ly_user_creation_dialog)
        )

        launchUserCreationDialog()
    }

    fun selectImageFromGallery(view: View) {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            launchGallery()
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                GALLERY_REQUEST_CODE
            )
        }
    }

    fun saveUser(view: View) {
        viewModel.saveUser()
    }

    private fun registerActivityResult() {
        galleryActivityResultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) {
                    val data = result.data?.data
                    viewModel.userImageUriCreation = data
                    ImageBinding.setImageUrl(dialogCreatorBinding?.btnImgAvatar!!, data.toString())
                }
            }
    }

    private fun launchGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        galleryActivityResultLauncher.launch(intent)
    }

    private fun launchUserCreationDialog() {
        setupUserCreationDialog()

        alertUserCreationBuilder.setView(dialogCreatorBinding?.root)
        alertUser = alertUserCreationBuilder.create()
        alertUser.show()
    }

    private fun setupUserCreationDialog() {
        dialogCreatorBinding?.view = this
        dialogCreatorBinding?.viewModel = viewModel
    }

    companion object {
        private const val GALLERY_REQUEST_CODE = 1
    }

}