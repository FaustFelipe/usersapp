package br.com.felipefaustini.usersapp.presentation.home

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.domain.usecases.home.IHomeUseCase
import br.com.felipefaustini.domain.utils.Result
import br.com.felipefaustini.usersapp.presentation.BaseViewModel
import br.com.felipefaustini.usersapp.utils.EventLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val homeUseCase: IHomeUseCase
) : BaseViewModel() {

    var userImageUriCreation: Uri? = null
    var username: String = ""
    var bio: String = ""

    private val _userSavedLiveData = EventLiveData<Unit>()
    val userSavedLiveData: LiveData<Unit> = _userSavedLiveData

    private val _usersListLiveData = MutableLiveData<List<User>>().apply {
        value = emptyList()
    }
    val userListLiveData: LiveData<List<User>> = _usersListLiveData

    override fun onInit() {
        listUsers()
    }

    override fun clearData() {
        clearDraftUser()
    }

    fun listUsers() {
        launchDataLoad {
            homeUseCase.getUsers().collect {
                when (val result = it) {
                    is Result.Success -> {
                        _usersListLiveData.postValue(result.data)
                    }
                    is Result.Error -> {
                        println("Error")
                    }
                }
            }
        }
    }

    fun saveUser() {
        launchDataLoad {
            homeUseCase.saveUser(username, (userImageUriCreation ?: "").toString(), bio).collect {
                when(val result = it) {
                    is Result.Success -> {
                        _userSavedLiveData.postValue(Unit)
                    }
                    is Result.Error -> {
                        println("Error")
                    }
                }
            }
        }
    }

    fun deleteUser(id: Long) {
        launchDataLoad {
            homeUseCase.deleteById(id).collect {
                when(val result = it) {
                    is Result.Success -> {

                    }
                    is Result.Error -> {
                        println("Error")
                    }
                }
            }
        }
    }

    private fun clearDraftUser() {
        userImageUriCreation = null
        username = ""
        bio = ""
    }

}