package br.com.felipefaustini.usersapp.presentation.home

import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.usersapp.R
import br.com.felipefaustini.usersapp.databinding.LyUserItemBinding
import br.com.felipefaustini.usersapp.utils.extensions.inflate

class HomeUsersAdapter(
    private val onClickListener: (id: Long) -> Unit,
    private val onDeleteUserListener: (id: Long) -> Unit
): ListAdapter<User, HomeUsersAdapter.UsersViewHolder>(
    HomeUsersDiff()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        return UsersViewHolder(parent.inflate(R.layout.ly_user_item))
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding?.user = item
        holder.binding?.root?.setOnLongClickListener {
            onDeleteUserListener.invoke(item.id)
            true
        }
        holder.binding?.root?.setOnClickListener {
            onClickListener.invoke(item.id)
        }
        holder.binding?.executePendingBindings()
    }

    fun setData(list: List<User>) {
        this.submitList(list.toMutableList())
    }
    
    inner class UsersViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val binding = DataBindingUtil.bind<LyUserItemBinding>(itemView)
    }
    
    class HomeUsersDiff: DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }
    }

}