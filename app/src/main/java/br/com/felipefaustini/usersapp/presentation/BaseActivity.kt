package br.com.felipefaustini.usersapp.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<A: ViewDataBinding, V: BaseViewModel>(
    @LayoutRes private val layoutRes: Int
): AppCompatActivity() {

    protected var binding: A? = null

    protected open val viewModel: V? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutRes)
        binding?.lifecycleOwner = this

        setupViews()
        setupObservables()
    }

    protected abstract fun setupViews()

    protected open fun setupObservables() {
        viewModel?.onInit()
    }

    protected fun showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, message, length).show()
    }

}