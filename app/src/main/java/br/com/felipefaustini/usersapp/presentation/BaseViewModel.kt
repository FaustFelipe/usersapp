package br.com.felipefaustini.usersapp.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

abstract class BaseViewModel: ViewModel() {

    protected val _loadingLiveData = MutableLiveData<Boolean>().apply {
        value = false
    }
    val loadingLiveData: LiveData<Boolean> = _loadingLiveData

    abstract fun onInit()

    protected fun launchDataLoad(block: suspend () -> Unit): Job {
        return viewModelScope.launch {
            try {
                _loadingLiveData.value = true
                block()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                _loadingLiveData.value = false
                clearData()
            }
        }
    }

    protected abstract fun clearData()

}