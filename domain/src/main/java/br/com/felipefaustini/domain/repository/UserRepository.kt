package br.com.felipefaustini.domain.repository

import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.domain.utils.Result
import kotlinx.coroutines.flow.Flow

interface UserRepository {
    fun saveUser(user: User): Flow<Result<Boolean>>
    fun getAllUsers(): Flow<Result<List<User>>>
    fun deleteById(id: Long): Flow<Result<Unit>>
}