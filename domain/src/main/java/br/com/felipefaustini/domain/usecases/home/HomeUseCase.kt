package br.com.felipefaustini.domain.usecases.home

import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.domain.repository.UserRepository
import br.com.felipefaustini.domain.utils.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class HomeUseCase @Inject constructor(
    private val repository: UserRepository
): IHomeUseCase {

    override fun saveUser(name: String, avatar: String, bio: String): Flow<Result<Boolean>> {
        val user = User(name = name, avatar = avatar, bio = bio)
        return repository.saveUser(user)
    }

    override fun getUsers(): Flow<Result<List<User>>> {
        return repository.getAllUsers()
    }

    override fun deleteById(id: Long): Flow<Result<Unit>> {
        return repository.deleteById(id)
    }

}