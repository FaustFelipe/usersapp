package br.com.felipefaustini.domain.models

data class User(
    val id: Long = -1,
    val name: String,
    val avatar: String,
    val bio: String
)