package br.com.felipefaustini.domain.usecases.home

import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.domain.utils.Result
import kotlinx.coroutines.flow.Flow

interface IHomeUseCase {
    fun saveUser(name: String, avatar: String, bio: String): Flow<Result<Boolean>>
    fun getUsers(): Flow<Result<List<User>>>
    fun deleteById(id: Long): Flow<Result<Unit>>
}