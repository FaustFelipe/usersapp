package br.com.felipefaustini.domain.usecases.home

import br.com.felipefaustini.domain.models.User
import br.com.felipefaustini.domain.repository.UserRepository
import br.com.felipefaustini.domain.utils.ErrorEntity
import br.com.felipefaustini.domain.utils.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify
import org.mockito.kotlin.verifyNoMoreInteractions
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class HomeUseCaseTest {

    @Mock
    private lateinit var repository: UserRepository

    private lateinit var homeUseCase: HomeUseCase

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun beforeEachTest() {
        homeUseCase = HomeUseCase(repository)
    }

    @Test
    fun saveUser_shouldReturnASuccessFlow() = dispatcher.runBlockingTest {
        val expected = Result.Success(true)
        val user = User(
            name = "Felipe",
            avatar = "",
            bio = "bio"
        )

        whenever(repository.saveUser(user)).thenReturn(flowOf(Result.Success(true)))

        val result = homeUseCase.saveUser(user.name, user.avatar, user.bio).first()

        verify(repository).saveUser(user)
        verifyNoMoreInteractions(repository)
        assertEquals(expected, result)
    }

    @Test
    fun saveUser_shouldReturnAnErrorEntityFlow() = dispatcher.runBlockingTest {
        val expected = Result.Error(ErrorEntity.Unknown)
        val user = User(
            name = "Felipe",
            avatar = "",
            bio = "bio"
        )

        whenever(repository.saveUser(user)).thenReturn(flowOf(Result.Error(ErrorEntity.Unknown)))

        val result = homeUseCase.saveUser(user.name, user.avatar, user.bio).first()

        verify(repository).saveUser(user)
        verifyNoMoreInteractions(repository)
        assertEquals(expected, result)
    }

    @Test
    fun getAllUsers_shouldReturnAListOfUsers() = dispatcher.runBlockingTest {
        val user = User(
            name = "Felipe",
            avatar = "",
            bio = "bio"
        )
        val expected = Result.Success(listOf(user))

        whenever(repository.getAllUsers()).thenReturn(flowOf(Result.Success(listOf(user))))

        val result = homeUseCase.getUsers().first()

        verify(repository).getAllUsers()
        verifyNoMoreInteractions(repository)
        assertEquals(expected, result)
    }

}