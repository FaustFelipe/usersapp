package br.com.felipefaustini.providermodule.domain

import br.com.felipefaustini.domain.repository.UserRepository
import br.com.felipefaustini.domain.usecases.home.HomeUseCase
import br.com.felipefaustini.domain.usecases.home.IHomeUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DomainModuleDI {

    @Provides
    @Singleton
    fun provideHomeUseCase(repository: UserRepository): IHomeUseCase {
        return HomeUseCase(repository)
    }

}