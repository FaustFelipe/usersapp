package br.com.felipefaustini.providermodule.data

import br.com.felipefaustini.data.database.dao.UserDao
import br.com.felipefaustini.data.repository.UserRepositoryImpl
import br.com.felipefaustini.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModuleDI {

    @Provides
    @Singleton
    fun provideRepositoryModule(userDao: UserDao): UserRepository {
        return UserRepositoryImpl(userDao, Dispatchers.IO)
    }

}