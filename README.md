# UsersApp

An application to save users in local database

## Techologies:
- Hilt
- Room
- Mockito
- JUnit
- Clean Architecture
- Flow
- Kotlin
- LiveData
- Multimodule 
- MVVM
- Unit testing
